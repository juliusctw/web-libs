

function table_manage(parent_id){
    this.parent_id = parent_id
    this.cell_list = []
	this.class_self = this
	this.max_column = 2
	this.max_row = 2
	this.current_row = 1
	this.table_instance_name = ''
	this.max_column = 2
	this.max_row = 2

	//	html views
	this.item_view_html = ''		// keep the innerHTML while in expand view

	//	Styles
	this.top_content_style = 'width:100px;border:1px solid black;padding:10px;margin-top:20px;margin-left:20px;margin-bottom:20px;'
	this.content_style = 'width:100px;border:1px solid black;padding:10px;margin-top:20px;margin-left:20px;margin-bottom:20px;'
	this.inner_cell_div_style = 'border:1px solid grey;border-radius: 20px;'
	this.expanded_style = 'width:300px;border:1px solid black;padding:10px;margin-top:20px;margin-left:20px;margin-bottom:20px;'


	this.create_table = function(table_manager_name){
		this.table_instance_name = table_manager_name
		row_id = 'row_id_' + this.get_rand_num()

		new_cell = this.create_new_outer_cell_db(row_id)

		//	Table itself
		root_table  = '<table id="root_table" border=0 >'
		root_table += '<tr id="' + row_id + '">'
		root_table += 	'<td id="' + new_cell['outer_cell_id'] + '">'

		root_table += 		this.inner_table(new_cell)

		root_table += 	'</td>'
		root_table += '</tr>'
		root_table += '</table>'
		document.getElementById(this.parent_id).innerHTML = root_table

		this.innerTop_right_menu(new_cell)
		this.set_div_style(new_cell['top_content_id'] , this.top_content_style)
		this.set_div_style(new_cell['content_id'] , this.content_style)
		this.set_div_style(new_cell['inner_div_id'] , this.inner_cell_div_style)

		return new_cell
	}



	this.inner_table = function(new_cell){	


		root_table  = 		'<div id="' + new_cell['inner_div_id'] + '">'
		root_table  += 		'<table border=0 style="width:100%">'
		root_table  += 			'<tr>'
		root_table  +=				'<td>'
		root_table  += 					'<div id="' + new_cell['top_content_id'] + '">' + new_cell['inner_div_id'] + '</div>'
		root_table  +=				'</td>'
		root_table  +=				'<td>'
		root_table  +=					'<div id="' + new_cell['inner_top_right_div_id'] + '"></div>'
		root_table  += 				'</td>'
		root_table  += 			'</tr>'
		root_table  += 			'<tr>'
		root_table  +=				'<td colspan=2>'
		root_table  += 					'<div id="' + new_cell['content_id'] + '"></div>'
		root_table  +=				'</td>'
		root_table  += 			'</tr>'
		root_table  += 		'</table>'
		root_table  += 		'</div>'

		return root_table
	}

	this.innerTop_right_menu = function(cell_list_obj){
		mo = new Menu_obj(cell_list_obj['inner_top_right_div_id'])
		menu = mo.get_Menu()
		item_1 = mo.createMenuObj(menu, '<a href="#">Panel</a>', true)
			obj1 = mo.createMenuObj(item_1, '<a href="javascript:' + this.table_instance_name + '.add_cell()">Add</a>')
			obj2 = mo.createMenuObj(item_1, '<a href="javascript:' + this.table_instance_name + '.delete_cell(\'' + cell_list_obj['outer_cell_id'] + '\')">Delete</a>')
			obj3 = mo.createMenuObj(item_1, '<a href="javascript:' + this.table_instance_name + '.expand_view(\'' + cell_list_obj['outer_cell_id'] + '\')">Expand</a>')
	
		mo.set_style(item_1, "font-size: 15px;")
		
		mo.displayMenu()
	}


	this.innerTop_right_menu_Expanded_view = function(cell_list_obj){
		mo = new Menu_obj(cell_list_obj['inner_top_right_div_id'])
		menu = mo.get_Menu()
		item_1 = mo.createMenuObj(menu, '<a href="#">Panel</a>', true)
			obj3 = mo.createMenuObj(item_1, '<a href="javascript:' + this.table_instance_name + '.contract_view(\'' + cell_list_obj['outer_cell_id'] + '\')">Multi-view</a>')
	
		mo.set_style(item_1, "font-size: 15px;")
		
		mo.displayMenu()
	}

	this.contract_view = function(outer_cell_id){
		document.getElementById(this.parent_id).innerHTML = this.item_view_html

		id = this.find_element_by_outer_cell_id(outer_cell_id)
		document.getElementById(this.cell_list[id]['inner_top_right_div_id']).innerHTML = ''
		this.innerTop_right_menu(this.cell_list[id])
	}

	this.expand_view = function(outer_cell_id){
		this.item_view_html = document.getElementById(this.parent_id).innerHTML
		id = this.find_element_by_outer_cell_id(outer_cell_id)
		outer_cell_id = document.getElementById(this.cell_list[id]['outer_cell_id'])

		document.getElementById(this.parent_id).innerHTML = outer_cell_id.innerHTML
		document.getElementById(this.cell_list[id]['inner_top_right_div_id']).innerHTML = ''

		this.innerTop_right_menu_Expanded_view(this.cell_list[id])
	}

	this.delete_cell = function(outer_cell_id){
		//alert(outer_cell_id)

    	if(this.cell_list.length == 1){
			alert('You cannot delete the last panel')
			return
		}

		row_id = 'row_id_' + this.get_rand_num()
		root_table  = '<table id="root_table" border=1>'
		root_table += '<tr id="' + row_id + '">'
		root_table += '</tr>'
		root_table += '</table>'
		document.getElementById(this.parent_id).innerHTML = root_table

		id = this.find_element_by_outer_cell_id(outer_cell_id)
		this.cell_list.splice(id,1)
		this.refill_cell(row_id)

	}

	this.refill_cell = function(row_id){

		for(m=0; m<this.cell_list.length; m++){

			
			num_of_cells = document.getElementById(row_id).cells.length


			if((num_of_cells + 1) > this.max_column){
				new_row = document.getElementById('root_table').insertRow(-1)
				row_id = 'row_id_' + this.get_rand_num()
				new_row.setAttribute("id", row_id);
			}

			this.cell_list[m]['row_id'] = row_id
			current_cell = this.cell_list[m]
	
			new_cell_html_element = document.getElementById(row_id).insertCell(-1)
			new_cell_html_element.setAttribute("id", current_cell['outer_cell_id']);
			new_cell_html_element.innerHTML = this.inner_table(current_cell)
	
			this.innerTop_right_menu(current_cell)
			this.set_div_style(current_cell['top_content_id'] , this.top_content_style)
			this.set_div_style(current_cell['content_id'] , this.content_style)
			this.set_div_style(current_cell['inner_div_id'] , this.inner_cell_div_style)
		}

	}


	this.add_cell= function(){
		//	Check for new row
		last_element_of_cell_list = this.cell_list.slice(-1)[0]
		num_of_cells = document.getElementById(last_element_of_cell_list['row_id']).cells.length
		num_of_rows = document.getElementById('root_table').rows.length
		
		row_id = last_element_of_cell_list['row_id']
		if((num_of_cells + 1) > this.max_column){
			if((num_of_rows+1) > this.max_row){	// Limit the size by row
				alert('You have reached maximum amount of panels')
				return
			}

			new_row = document.getElementById('root_table').insertRow(-1)
			row_id = 'row_id_' + this.get_rand_num()
			new_row.setAttribute("id", row_id);
		}
		

		new_cell = this.create_new_outer_cell_db(row_id)

		new_cell_html_element = document.getElementById(row_id).insertCell(-1)
		new_cell_html_element.setAttribute("id", new_cell['outer_cell_id']);
		new_cell_html_element.innerHTML = this.inner_table(new_cell)

		this.innerTop_right_menu(new_cell)
		this.set_div_style(new_cell['top_content_id'] , this.top_content_style)
		this.set_div_style(new_cell['content_id'] , this.content_style)
		this.set_div_style(new_cell['inner_div_id'] , this.inner_cell_div_style)
	}

	this.set_div_style = function(div_id , style){
		document.getElementById(div_id).style = style
	}
	this.get_rand_num = function(){
		return (parseInt(10000000*Math.random())).toString()
	}
	this.create_new_outer_cell_db = function(row_id){
		new_cell = {}
		new_cell['content_id'] = 'content_id_' + this.get_rand_num()
		new_cell['top_content_id'] = 'top_content_id_' + this.get_rand_num()
		new_cell['row_id'] = row_id
		new_cell['outer_cell_id'] = 'outer_cell_id' + this.get_rand_num()
		new_cell['inner_top_right_div_id'] = 'inner_top_right_div_id_' + this.get_rand_num()
		new_cell['inner_div_id'] = 'inner_div_id_' + this.get_rand_num()
		this.cell_list = this.cell_list.concat(new_cell)
		return new_cell
	}
	
	this.find_element_by_outer_cell_id = function(outer_cell_id){
		for(m=0; m<this.cell_list.length; m++){
			if(outer_cell_id == this.cell_list[m]['outer_cell_id']){
				return m
			}
		}
	}

}

