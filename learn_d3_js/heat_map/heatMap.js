
function heat_map(svg_id, data){

	max_indx = d3.max(data, function (d) { return Math.max(d.i, d.j); })

	this.margin = { top: 0, right: 0, bottom: 0, left: 0 }
	this.container_width = 600;
	this.container_height = 600;
	this.canvas_width = this.container_width - this.margin.left - this.margin.right
	this.canvas_height = this.container_height - this.margin.top - this.margin.bottom
    this.each_heatMap_chart_class_name = 'each_heatMap_chart_class_name_' + Math.floor(Math.random()*1000000)
	this.data = data

	gridSize = Math.floor(this.canvas_width / max_indx),
	legendElementWidth = gridSize*2,

	buckets = 9,
	colors = ["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"], // alternatively colorbrewer.YlGnBu[9]

	this.draw = function(){
		this.svg = d3.select(svg_id).append("svg")
		    .attr("width", this.canvas_width + this.margin.left + this.margin.right)
		    .attr("height", this.canvas_height + this.margin.top + this.margin.bottom)
		    .append("g")
		    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
	
		colorScale = d3.scale.quantile()
	              .domain([0, buckets - 1, d3.max(this.data, function (d) { return d.value; })])
	              .range(colors);
	
          var cards = this.svg.selectAll(this.each_heatMap_chart_class_name)
              .data(this.data, function(d) { return d.i+':'+d.j;});

          cards.append("title");

          cards.enter().append("rect")
              .attr("x", function(d) { return (d.j- 1) * gridSize; })
              .attr("y", function(d) { return (d.i- 1) * gridSize; })
              .attr("rx", 4)
              .attr("ry", 4)
              .attr("class", "hour bordered")
              .attr("width", gridSize)
              .attr("height", gridSize)
              .style("fill", colors[0]);

          cards.transition().duration(1000)
              .style("fill", function(d) { return colorScale(d.value); });

          cards.select("title").text(function(d) { return d.value; });
          
          cards.exit().remove();
	}

	this.i_labels = function(labels){
    	this.i_label = 'i_label_' + Math.floor(Math.random()*1000000)

	      dayLabels = this.svg.selectAll(this.i_label)
			.data(labels)
			.enter().append("text")
            .text(function (d) { return d; })
            .attr("x", 0)
            .attr("y", function (d, i) { return i * gridSize; })
            .style("text-anchor", "end")
            .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
            .attr("class", function (d, i) { return ((i >= 0 && i <= 4) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis"); });
	}

	this.j_labels = function(labels){
    	this.j_label = 'j_label_' + Math.floor(Math.random()*1000000)

		var timeLabels = this.svg.selectAll(this.j_label)
			.data(times)
			.enter().append("text")
			.text(function(d) { return d; })
			.attr("x", function(d, i) { return i * gridSize; })
			.attr("y", 0)
			.style("text-anchor", "middle")
			.attr("transform", "translate(" + gridSize / 2 + ", -6)")
			.attr("class", function(d, i) { return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis"); });
	}

	this.legend = function(){
    	this.legend = 'legend_' + Math.floor(Math.random()*1000000)

		var legend = this.svg.selectAll(this.legend)
		    .data([0].concat(colorScale.quantiles()), function(d) { return d; });
		
		legend.enter().append("g")
		    .attr("class", "legend");
		
		legend.append("rect")
			.attr("x", function(d, i) { return legendElementWidth * i; })
			.attr("y", this.canvas_height)
			.attr("width", legendElementWidth)
			.attr("height", gridSize / 2)
			.style("fill", function(d, i) { return colors[i]; });
		
		legend.append("text")
		  .attr("class", "mono")
		  .text(function(d) { return "≥ " + Math.round(d); })
		  .attr("x", function(d, i) { return legendElementWidth * i; })
		  .attr("y", this.canvas_height + gridSize);
		
		legend.exit().remove();
	}
	this.compress_map = function(max_block_i=50, max_block_j=50){
		Heat_Map_list = []
		data_matrix = []

		RowDic = {}
		//	Create the matrix
		for(a=0 ;a < this.data.length ;a++){
			i = this.data[a].i
			j = this.data[a].j
			v = this.data[a].value

			if(RowDic[i] == undefined){
				RowDic[i] = []
			}
			RowDic[i] = RowDic[i].concat(v)
		}

		for(a=0; a < Object.keys(RowDic).length +2; a++){
			if(RowDic[a] != undefined){
				data_matrix.push(RowDic[a])
			}
		}


		ratio_i = data_matrix.length/max_block_i
		ratio_j = RowDic[i].length/max_block_j

		//	Compression
		for(k=0;k < max_block_i;k++){
			for(l=0;l < max_block_j;l++){


				v_list = []
				for(s=Math.floor(k*ratio_i) ;s < Math.round(k*ratio_i+ratio_i) ;s++){
					for(t= Math.floor(l*ratio_j) ;t < Math.round(l*ratio_j+ratio_j);t++){
						v_list = v_list.concat(data_matrix[s][t])
					}
				}
				addup = v_list.reduce(function(a, b) { return a + b; }, 0)
				avg_value = addup/v_list.length

				item = {}
				item['i'] = k + 1
				item['j'] = l + 1
				item['value'] = avg_value

				Heat_Map_list.push(item)
			}
		}
		this.data = Heat_Map_list
	}
}
