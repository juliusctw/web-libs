
all_colors = d3.scale.category10()

function draw_scatter(svg_id){
	this.svg_id = svg_id
	this.margin = {top: 30, right: 80, bottom: 60, left: 80},
	this.container_width = 800;
	this.container_height = 500;
    this.canvas_width = this.container_width - this.margin.left - this.margin.right,
    this.canvas_height = this.container_height - this.margin.top - this.margin.bottom;
    this.x_axis_title = 'x axis'
    this.y_axis_title = 'y axis'

    this.x_axis_class_name = 'x_axis_' + Math.floor(Math.random()*1000000)
    this.y_axis_class_name = 'y_axis_' + Math.floor(Math.random()*1000000)

    this.scatter_chart_class_name = 'scatter_chart_' + Math.floor(Math.random()*1000000)
    this.each_circle_class_name = 'each_scatter_' + Math.floor(Math.random()*1000000)

    this.y_num_of_levels = 10
    this.circle_radius = 6
    this.y_tick_label = ''


	this.circle_click = function(data){
		//console.log(data)
	}
	this.circle_mouse_over = function(data){
		//console.log(data)
	}

	this.initialize = function(){
		this.x = d3.scale.linear()				//		this.x = d3.scale.ordinal()
		    .range([0, this.canvas_width]);     //		    .rangeRoundBands([0, this.canvas_width], .1);

		this.y = d3.scale.linear()
		    .range([this.canvas_height , 0]);
	
		this.xAxis = d3.svg.axis()
		    .scale(this.x)
		    .orient("bottom");
		
		this.yAxis = d3.svg.axis()
		    .scale(this.y)
		    .orient("left")
		    .ticks(this.y_num_of_levels, this.y_tick_label);
	
	
		this.scatter_chart = d3.select(this.svg_id) //".svg2"
			.attr('width', this.container_width)
			.attr('height', this.container_height)
			.attr('class', this.scatter_chart_class_name)


		this.scatter_chart.append("g")
		    .attr("class", this.x_axis_class_name)
		    .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + this.canvas_height) + ")")
			.append("text") // just for the title (ticks are automatic)
		    .attr("x", this.canvas_width/2)
		    .attr("y", this.margin.bottom - 10)
		    .attr("dy", ".0em")
		    .text(this.x_axis_title);


		this.scatter_chart.append("g")
		    .attr("class", this.y_axis_class_name)
		    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
			.append("text") // just for the title (ticks are automatic)
		    .attr("transform", "rotate(-90)") // rotate the text!
		    .attr("x", -(this.canvas_height/2)-this.margin.top)
		    .attr("y", -this.margin.left/1.5)
		    .attr("dy", ".0em")
		    .text(this.y_axis_title);
	}

	this.update = function(input_dat){

		x_axis_values = []	
		y_axis_values = []	
		input_dat.forEach( function(item,index){ 
			x_axis_values.push( item.magnitude[0] )
			y_axis_values.push( item.magnitude[1] )
		})

		this.x.domain([d3.min(x_axis_values, function(d) { return d; }), d3.max(x_axis_values, function(d) { return d; })]);
		this.y.domain([d3.min(y_axis_values, function(d) { return d; }), d3.max(y_axis_values, function(d) { return d; })]);
		this.scatter_chart.select("." + this.x_axis_class_name).transition().duration(300).call(this.xAxis);
		this.scatter_chart.select("." + this.y_axis_class_name).transition().duration(300).call(this.yAxis)
		
		// THIS IS THE ACTUAL WORKd!
		this.all_circles = this.scatter_chart.selectAll("." + this.each_circle_class_name)
					.data(input_dat) // (data) is an array/iterable thing, second argument is an ID generator function

		this.all_circles.exit()
		  .transition()
		  .duration(300)
		  .style('fill-opacity', 1e-6)
		  .remove();

		//d3.selectAll('.' + this.each_circle_class_name).remove();

		namespace_x = this.x;
		namespace_y = this.y;
		color_callback = this.color_callback


		this.all_circles.enter().append("circle")
			.attr("class", this.each_circle_class_name)
			.attr("cx", function (d,i) { return namespace_x(d.magnitude[0]); } )
			.attr("cy", function (d) { return namespace_y(d.magnitude[1]); } )
			.attr("r", this.circle_radius)
			.style('fill', this.color_callback)
			.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
			.on("click", this.circle_click)
			.on("mouseover", this.circle_mouse_over)
			.append("title")
			.text(function(d) { return d.magnitude[0].toFixed(2) + ' , ' + d.magnitude[1].toFixed(2) });

		this.all_circles
			.transition()  // Transition from old to new
			.duration(500)  // Length of animation
			.attr("cx", function (d,i) { return namespace_x(d.magnitude[0]); } )
			.attr("cy", function (d) { return namespace_y(d.magnitude[1]); } )
			.style('fill', this.color_callback)

	}

	this.color_callback = function(item,idx){
		if ('cluster' in item) {
			return all_colors(item.cluster)
		}else return all_colors(0)
	}
}

