var data = [
{'letter':1,  'magnitude' : [ -1.014579,   0.130567]} , 
{'letter':2,  'magnitude' : [  0.039681,  -0.661445]} , 
{'letter':3,  'magnitude' : [  0.718238,   0.879958]} , 
{'letter':4,  'magnitude' : [  0.842208,  -0.798141]} , 
{'letter':5,  'magnitude' : [  0.245613,   0.762816]} ,
{'letter':6,  'magnitude' : [ -0.047197,  -0.138953]} ,
{'letter':7,  'magnitude' : [ -0.362301,   0.310101]} ,
{'letter':8,  'magnitude' : [ -0.219598,   0.906047]} ,
{'letter':9,  'magnitude' : [ -0.731793,   0.517308]} ,
{'letter':10, 'magnitude' : [  0.498229,   0.206321]} ,
{'letter':11, 'magnitude' : [  4.312781,   3.777008]} ,
{'letter':12, 'magnitude' : [  4.484081,   4.347066]} ,
{'letter':13, 'magnitude' : [  4.605210,   4.381367]} ,
{'letter':14, 'magnitude' : [  3.443824,   4.032347]} ,
{'letter':15, 'magnitude' : [  4.628112,   3.302830]} ,
{'letter':16, 'magnitude' : [  3.845599,   4.035115]} ,
{'letter':17, 'magnitude' : [  4.847021,   3.825508]} ,
{'letter':18, 'magnitude' : [  3.845032,   3.743417]} ,
{'letter':19, 'magnitude' : [  3.561708,   3.734906]} ,
{'letter':20, 'magnitude' : [  4.533638,   3.661425]} ];
   
var margin = {top: 20, right: 15, bottom: 60, left: 60}
  , width = 960 - margin.left - margin.right
  , height = 500 - margin.top - margin.bottom;

var x = d3.scale.linear()
          .domain([d3.min(data, function(d) { return d[0]; }), d3.max(data, function(d) { return d[0]; })])
          .range([ 0, width ]);

var y = d3.scale.linear()
	      .domain([d3.min(data, function(d) { return d[1]; }), d3.max(data, function(d) { return d[1]; })])
	      .range([ height, 0 ]);

var chart = d3.select('body')
.append('svg:svg')
.attr('width', width + margin.right + margin.left)
.attr('height', height + margin.top + margin.bottom)
.attr('class', 'chart')

var main = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
.attr('width', width)
.attr('height', height)
.attr('class', 'main')   
    
// draw the x axis
var xAxis = d3.svg.axis()
.scale(x)
.orient('bottom');

main.append('g')
.attr('transform', 'translate(0,' + height + ')')
.attr('class', 'main axis date')
.call(xAxis);

// draw the y axis
var yAxis = d3.svg.axis()
.scale(y)
.orient('left');

main.append('g')
.attr('transform', 'translate(0,0)')
.attr('class', 'main axis date')
.call(yAxis);

var g = main.append("svg:g"); 

g.selectAll("scatter-dots")
  .data(data)
  .enter().append("svg:circle")
      .attr("cx", function (d,i) { return x(d[0]); } )
      .attr("cy", function (d) { return y(d[1]); } )
      .attr("r", 8);

all_circles = d3.selectAll("circle")[0]
for(i = 10; i < all_circles.length; i++){
	all_circles[i].style["fill"] = "#FF3333"
}

