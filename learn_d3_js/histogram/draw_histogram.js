
draw_histogram.prototype = new draw_bar_chart();       
draw_histogram.prototype.constructor=draw_histogram;      
function draw_histogram(svg_id){ 
	this.svg_id = svg_id
	this.segments = 10
	this.use_percentage = false
	this.count_list = {}
	this.histogram_dat = {}
	this.increment = 0

	this.calc_histogram = function(data){		// data = [2 ,3 1, 2, ...]
		max_val = Math.max.apply(Math, data)
		min_val = Math.min.apply(Math, data)
	
		incre = (max_val - min_val)/(this.segments - 1)

		count_list = {}				//  {0: 5, 1: 4, 2: 1, 3: 0, 4: 2, 5: 2, 6: 3, 7: 2, 8: 0, 9: 1} histogram count
		item_count = {}
		histogram_dat = []

		data.forEach( function(item,index){ 

			bin = Math.floor((item - min_val)/incre)
			if (bin in count_list){
				count_list[bin] = count_list[bin] + 1
			}else{
				count_list[bin] = 1
				item_count[bin] = []
			}
			item_count[bin].push(index)
		} ) 

		if(this.use_percentage){
			for (var key in count_list) {
			    // skip loop if the property is from prototype
			    if (!count_list.hasOwnProperty(key)) continue;
			
				count_list[key] = count_list[key]/data.length
			}
		}

		for(m = 0; m < this.segments; m++){
			tmp_input = {}
			//tmp_input['letter'] = parseFloat(m*incre+min_val).toFixed(2) + ' - ' + parseFloat((m+1)*incre+min_val).toFixed(2)
			tmp_input['letter'] = parseFloat((m+1)*incre+min_val).toFixed(2)
			tmp_input['item_list'] = item_count[m]

			if (m in count_list){
				tmp_input['magnitude'] = count_list[m]
				histogram_dat.push(tmp_input) 
			}else{
				count_list[m] = 0
				tmp_input['magnitude'] = 0
				histogram_dat.push(tmp_input) 
			}
		}

		this.count_list = count_list
		this.histogram_dat = histogram_dat
		this.increment = incre

		return histogram_dat
	}

	this.update = function(input_dat){
		this.calc_histogram(input_dat)
		draw_histogram.prototype.update.call(this, histogram_dat)
	}
} 

