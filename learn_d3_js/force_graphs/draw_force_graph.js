
function draw_force_graph(svg_id){
	this.svg_id = svg_id
	this.margin = {top: 30, right: 80, bottom: 60, left: 80},
	this.container_width = 800;
	this.container_height = 500;
    this.canvas_width = this.container_width - this.margin.left - this.margin.right,
    this.canvas_height = this.container_height - this.margin.top - this.margin.bottom;
    this.x_axis_title = 'x axis'
    this.y_axis_title = 'y axis'

    this.link_class_name = 'link_name_' + Math.floor(Math.random()*1000000)
    //this.y_axis_class_name = 'y_axis_' + Math.floor(Math.random()*1000000)
    this.bar_chart_class_name = 'bar_chart_' + Math.floor(Math.random()*1000000)
    this.each_force_class_name = 'each_bar_' + Math.floor(Math.random()*1000000)

    this.y_num_of_levels = 10
    this.y_tick_label = ''

	//this.bar_color = '#959595'
	//this.bar_click = function(data){
	//	console.log(data)
	//}
	//this.bar_mouse_over = function(data){
	//	console.log(data)
	//}

	this.initialize = function(){

//		this.x = d3.scale.ordinal()
//		    .rangeRoundBands([0, this.canvas_width], .1);
//		this.y = d3.scale.linear()
//		    .range([this.canvas_height , 0]);
//	
//		this.xAxis = d3.svg.axis()
//		    .scale(this.x)
//		    .orient("bottom");
//		
//		this.yAxis = d3.svg.axis()
//		    .scale(this.y)
//		    .orient("left")
//		    .ticks(this.y_num_of_levels, this.y_tick_label);
//	
//	
		this.force_graph = d3.select(this.svg_id) //".svg2"
			.attr('width', this.container_width)
			.attr('height', this.container_height)
			.attr('class', this.bar_chart_class_name)

		this.color = d3.scale.category20();
//
//		this.force_graph.append("g")
//		    .attr("class", this.x_axis_class_name)
//		    .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + this.canvas_height) + ")")
//			.append("text") // just for the title (ticks are automatic)
//		    .attr("x", this.canvas_width/2)
//		    .attr("y", this.margin.bottom - 10)
//		    .attr("dy", ".0em")
//		    .text(this.x_axis_title);
//
//
//		this.force_graph.append("g")
//		    .attr("class", this.y_axis_class_name)
//		    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
//			.append("text") // just for the title (ticks are automatic)
//		    .attr("transform", "rotate(-90)") // rotate the text!
//		    .attr("x", -(this.canvas_height/2)-this.margin.top)
//		    .attr("y", -this.margin.left/1.5)
//		    .attr("dy", ".0em")
//		    .text(this.y_axis_title);
	}

	this.update = function(input_dat){

		nodes = input_dat.nodes,
	    links = input_dat.links;

		this.force = d3.layout.force()
		    .charge(-120)
		    .linkDistance(function(d) { return d.value; })
		    .size([this.container_width, this.container_height])
			.nodes(input_dat.nodes)
			.links(input_dat.links)
			.start();

//		this.force.exit()
//		  .transition()
//		    .duration(300)
//		  .attr("height", this.canvas_height - this.y(0))
//		  .style('fill-opacity', 1e-6)
//		  .remove();

		link = this.force_graph.selectAll("." + this.link_class_name)
			.data(input_dat.links)

		link
			.exit()
			.transition()
		    .duration(300)
		  	.remove();

		link
			.enter().append("line")
			.attr("class", this.link_class_name)
			.style("stroke-width", 2)
			.style("stroke", '#999')
			.style("stroke-opacity", '0.6')

		tmp_color = this.color

		node = this.force_graph.selectAll(".node")
			.data(input_dat.nodes)

		node	
			.exit()
			.transition()
		    .duration(300)
			.attr("r", 8)
			.style("fill", function(d) { console.log('remove'); return tmp_color(d.group); })
		  	.remove();

		node
			.enter().append("circle")
			.attr("class", "node")
			.attr("r", 8)
			.style("fill", function(d) { console.log('add'); return tmp_color(d.group); })
			.call(this.force.drag)

		node.append("text")
			.attr("text-anchor", "middle")
			.attr('style', 'color:white;')
			.text(function(d) {
			  return d.name;
			});

		node.append("title")
			.text(function(d) { return d.name; });

		node.on("click", function() {
			node.style("fill", '#999999')
			sso = d3.select(this)
			group_id = sso[0][0].__data__.group
			//console.log(sso[0][0].__data__.group)
			
			for(i = 0; i < node[0].length;i++){
				//console.log(node[0][i].__data__.group)
				if(node[0][i].__data__.group == group_id){
					node[0][i].style['fill'] = "rgb(150, 150, 255)"
				}
			}
		  //d3.select(this).attr("r", 12);
		});

		this.force.on("tick", function() {
		  link.attr("x1", function(d) { return d.source.x; })
		      .attr("y1", function(d) { return d.source.y; })
		      .attr("x2", function(d) { return d.target.x; })
		      .attr("y2", function(d) { return d.target.y; });
		  node.attr("cx", function(d) { return d.x; })
		      .attr("cy", function(d) { return d.y; });
		});





		// THIS IS THE ACTUAL WORKd!
		//this.bars = this.force_graph.selectAll("." + this.each_force_class_name)
					//.data(input_dat, function(d) { return d.letter; }) // (data) is an array/iterable thing, second argument is an ID generator function

//		var force = d3.layout.force()
//		    .charge(-120)
//		    //.linkDistance(function(d) { return Math.sqrt(d.value); })
//		    .linkDistance(function(d) { return d.value; })
//		    .size([this.container_width, this.container_height])
//		      .nodes(nodes)
//		      .links(links)
//		      .start();
//
//  var node = this.force_graph.selectAll(".node")
//      .data(graph_info.nodes)
//    .enter().append("circle")
//      .attr("class", "node")
//      .attr("r", 8)
//      .style("fill", function(d) { return color(d.group); })
//      .call(force.drag)


//		this.force = d3.layout.force()
//		    .size([this.container_width, this.container_height])
//		    .nodes(nodes)
//		    .links(links);
//
//		this.force.linkDistance(width/3.5);

//		link = svg.selectAll('.link')
//	    .data(links)
//	    .enter().append('line')
//	    .attr('class', 'link')
//	    .attr('x1', function(d) { return nodes[d.source].x; })
//	    .attr('y1', function(d) { return nodes[d.source].y; })
//	    .attr('x2', function(d) { return nodes[d.target].x; })
//	    .attr('y2', function(d) { return nodes[d.target].y; });




//		this.bars.exit()
//		  .transition()
//		    .duration(300)
//		  .attr("y", this.y(0))
//		  .attr("height", this.canvas_height - this.y(0))
//		  .style('fill-opacity', 1e-6)
//		  .remove();
//		
//		// data that needs DOM = enter() (a set/selection, not an event!)
//		this.bars.enter().append("rect")
//			.attr("class", this.each_force_class_name)
//			.attr("y", this.y(0))
//			.style('fill', this.bar_color)
//			.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
//			.attr("height", this.canvas_height - this.y(0))
//			.on("click", this.bar_click)
//			.on("mouseover", this.bar_mouse_over)
//	
//		// the "UPDATE" set:
//		namespace_x = this.x;
//		namespace_y = this.y;
//		namespace_height = this.canvas_height;
//		this.bars.transition().duration(300).attr("x", function(d) { return namespace_x(d.letter); }) // (d) is one item from the data array, x is the scale object from above
//		  .attr("width", this.x.rangeBand()) // constant, so no callback function(d) here
//		  .attr("y", function(d) { return namespace_y(d.magnitude); })
//		  .attr("height", function(d) { return namespace_height - namespace_y(d.magnitude); }); // flip the height, because y's domain is bottom up, but SVG renders top down
	}
}

