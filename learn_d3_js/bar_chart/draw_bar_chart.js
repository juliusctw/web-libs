
function draw_bar_chart(svg_id){
	this.svg_id = svg_id
	this.margin = {top: 30, right: 80, bottom: 60, left: 80},
	this.container_width = 800;
	this.container_height = 500;
    this.canvas_width = this.container_width - this.margin.left - this.margin.right,
    this.canvas_height = this.container_height - this.margin.top - this.margin.bottom;
    this.x_axis_title = 'x axis'
    this.y_axis_title = 'y axis'

    this.x_axis_class_name = 'x_axis_' + Math.floor(Math.random()*1000000)
    this.y_axis_class_name = 'y_axis_' + Math.floor(Math.random()*1000000)
    this.bar_chart_class_name = 'bar_chart_' + Math.floor(Math.random()*1000000)
    this.each_bar_class_name = 'each_bar_' + Math.floor(Math.random()*1000000)

    this.y_num_of_levels = 10
    this.y_tick_label = ''

	this.bar_color = '#959595'
	this.bar_click = function(data){
		console.log(data)
	}
	this.bar_mouse_over = function(data){
		console.log(data)
	}

	this.initialize = function(){

		this.x = d3.scale.ordinal()
		    .rangeRoundBands([0, this.canvas_width], .1);
		this.y = d3.scale.linear()
		    .range([this.canvas_height , 0]);
	
		this.xAxis = d3.svg.axis()
		    .scale(this.x)
		    .orient("bottom");
		
		this.yAxis = d3.svg.axis()
		    .scale(this.y)
		    .orient("left")
		    .ticks(this.y_num_of_levels, this.y_tick_label);
	
	
		this.bar_chart = d3.select(this.svg_id) //".svg2"
			.attr('width', this.container_width)
			.attr('height', this.container_height)
			.attr('class', this.bar_chart_class_name)


		this.bar_chart.append("g")
		    .attr("class", this.x_axis_class_name)
		    .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + this.canvas_height) + ")")
			.append("text") // just for the title (ticks are automatic)
		    .attr("x", this.canvas_width/2)
		    .attr("y", this.margin.bottom - 10)
		    .attr("dy", ".0em")
		    .text(this.x_axis_title);


		this.bar_chart.append("g")
		    .attr("class", this.y_axis_class_name)
		    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
			.append("text") // just for the title (ticks are automatic)
		    .attr("transform", "rotate(-90)") // rotate the text!
		    .attr("x", -(this.canvas_height/2)-this.margin.top)
		    .attr("y", -this.margin.left/1.5)
		    .attr("dy", ".0em")
		    .text(this.y_axis_title);
	}

	this.update = function(input_dat){
		this.x.domain(input_dat.map(function(d) { return d.letter; }));
		this.y.domain([0, d3.max(input_dat, function(d) { return d.magnitude; })]);
		this.bar_chart.select("." + this.x_axis_class_name).transition().duration(300).call(this.xAxis);
		this.bar_chart.select("." + this.y_axis_class_name).transition().duration(300).call(this.yAxis)
		
		// THIS IS THE ACTUAL WORKd!
		this.bars = this.bar_chart.selectAll("." + this.each_bar_class_name)
					.data(input_dat, function(d) { return d.letter; }) // (data) is an array/iterable thing, second argument is an ID generator function

		this.bars.exit()
		  .transition()
		    .duration(300)
		  .attr("y", this.y(0))
		  .attr("height", this.canvas_height - this.y(0))
		  .style('fill-opacity', 1e-6)
		  .remove();
		
		// data that needs DOM = enter() (a set/selection, not an event!)
		this.bars.enter().append("rect")
			.attr("class", this.each_bar_class_name)
			.attr("y", this.y(0))
			.style('fill', this.bar_color)
			.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
			.attr("height", this.canvas_height - this.y(0))
			.on("click", this.bar_click)
			.on("mouseover", this.bar_mouse_over)
	
		// the "UPDATE" set:
		namespace_x = this.x;
		namespace_y = this.y;
		namespace_height = this.canvas_height;
		this.bars.transition().duration(300).attr("x", function(d) { return namespace_x(d.letter); }) // (d) is one item from the data array, x is the scale object from above
		  .attr("width", this.x.rangeBand()) // constant, so no callback function(d) here
		  .attr("y", function(d) { return namespace_y(d.magnitude); })
		  .attr("height", function(d) { return namespace_height - namespace_y(d.magnitude); }); // flip the height, because y's domain is bottom up, but SVG renders top down
	}
}

